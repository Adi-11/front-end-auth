import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'front-end-auth';
  isAuthenticated: boolean;

  constructor(public authService: AuthService) {
    this.authService.isAuthencticated.subscribe(
      (isAuthenticated: boolean) => (this.isAuthenticated = isAuthenticated)
    );
  }

  async ngOnInit() {
    this.isAuthenticated = await this.authService.chechAuthenticated();
  }

  logout() {
    this.authService.logout('/');
  }
}
